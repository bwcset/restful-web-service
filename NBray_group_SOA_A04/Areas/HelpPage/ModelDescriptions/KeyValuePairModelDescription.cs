namespace NBray_group_SOA_A04.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// 
    /// </summary>
    public class KeyValuePairModelDescription : ModelDescription
    {
        /// <summary>
        /// 
        /// </summary>
        public ModelDescription KeyModelDescription { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ModelDescription ValueModelDescription { get; set; }
    }
}
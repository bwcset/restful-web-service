using System;

namespace NBray_group_SOA_A04.Areas.HelpPage.ModelDescriptions
{
    /// <summary>
    /// 
    /// </summary>
    public class ParameterAnnotation
    {
        /// <summary>
        /// 
        /// </summary>
        public Attribute AnnotationAttribute { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Documentation { get; set; }
    }
}
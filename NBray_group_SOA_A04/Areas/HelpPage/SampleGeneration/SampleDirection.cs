namespace NBray_group_SOA_A04.Areas.HelpPage
{
    /// <summary>
    /// Indicates whether the sample is used for request or response
    /// </summary>
    public enum SampleDirection
    {
        /// <summary>
        /// 
        /// </summary>
        Request = 0,
        /// <summary>
        /// 
        /// </summary>
        Response
    }
}
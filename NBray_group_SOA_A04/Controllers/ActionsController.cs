﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System.Collections.Generic;
using System.Web.Mvc;
using NBray_group_SOA_A04.Models;

namespace NBray_group_SOA_A04.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ActionsController : Controller
    {
        // GET: Actions
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Insert()
        {
            ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");
            ViewBag.ordersAPI = Url.Content("~/api/orders/");
            ViewBag.cartsAPI = Url.Content("~/api/carts/");
            return View();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Update()
        {
            ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");
            ViewBag.ordersAPI = Url.Content("~/api/orders/");
            ViewBag.cartsAPI = Url.Content("~/api/carts/");
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Delete()
        {
            ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");
            ViewBag.ordersAPI = Url.Content("~/api/orders/");
            ViewBag.cartsAPI = Url.Content("~/api/carts/");
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Search()
        {
            ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");
            ViewBag.ordersAPI = Url.Content("~/api/orders/");
            ViewBag.cartsAPI = Url.Content("~/api/carts/");
            ViewBag.customerOrderAPI = Url.Content("~/api/custOrders/");

            return View();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Results()
        {
            /*ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");*/
            ViewBag.purchaseOrderAPI = Url.Content("~/api/purchaseOrders/");
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult DisplayOrders()
        {
            /*ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");*/
            ViewBag.purchaseOrderAPI = Url.Content("~/api/purchaseOrders/");
            return View();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult PreUpdateSearch()
        {
            ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");
            ViewBag.ordersAPI = Url.Content("~/api/orders/");
            ViewBag.cartsAPI = Url.Content("~/api/carts/");
            return View();
        }

        // GET: Actions
        [HttpGet]
        public ActionResult PreUpdateSearchCustomer([System.Web.Http.FromBody] CustomerDTO customer)
        {
            ProductDTO p = new ProductDTO();
            OrderDTO o = new OrderDTO();
            CartDTO ca = new CartDTO();

            TempData["customer"] = customer;
            TempData["product"] = p;
            TempData["order"] = o;
            TempData["cart"] = ca;

            return View("PreUpdateSearch");
        }

        // GET: Actions
        [HttpGet]
        public ActionResult PreUpdateSearchProduct([System.Web.Http.FromBody] ProductDTO product)
        {
            CustomerDTO c = new CustomerDTO();
            OrderDTO o = new OrderDTO();
            CartDTO ca = new CartDTO();

            TempData["customer"] = c;
            TempData["product"] = product;
            TempData["order"] = o;
            TempData["cart"] = ca;

            return View("PreUpdateSearch");
        }

        // GET: Actions
        [HttpGet]
        public ActionResult PreUpdateSearchOrder([System.Web.Http.FromBody] OrderDTO order)
        {
            CustomerDTO c = new CustomerDTO();
            ProductDTO p = new ProductDTO();
            CartDTO ca = new CartDTO();

            TempData["customer"] = c;
            TempData["product"] = p;
            TempData["order"] = order;
            TempData["cart"] = ca;

            return View("PreUpdateSearch");
        }

        // GET: Actions
        [HttpGet]
        public ActionResult PreUpdateSearchCart([System.Web.Http.FromBody] CartDTO cart)
        {
            CustomerDTO c = new CustomerDTO();
            ProductDTO p = new ProductDTO();
            OrderDTO o = new OrderDTO();

            TempData["customer"] = c;
            TempData["product"] = p;
            TempData["order"] = o;
            TempData["cart"] = cart;

            return View("PreUpdateSearch");
        }


        // GET: Actions
        [HttpGet]
        public ActionResult DisplayOrdersSearchBody([System.Web.Http.FromBody] string orderId)
        {
            TempData["orderId"] = orderId;

            return View("DisplayOrders");
        }
        
        // GET: Actions
        [HttpPost]
        public ActionResult CustomerOrderSearch(IList<CustOrderDTO> customerOrders)
        {
            TempData["custOrders"] = customerOrders;
            return View("Search");
        }

    }
}
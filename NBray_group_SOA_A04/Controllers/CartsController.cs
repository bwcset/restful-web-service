﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NBray_group_SOA_A04.Models;

namespace NBray_group_SOA_A04.Controllers
{
    [RoutePrefix("api")]
    public class CartsController : ApiController
    {
        private static readonly ProductRepository productRepository = new ProductRepository();
        private static readonly OrderRepository orderRepository = new OrderRepository();
        private static readonly CartRepository cartRepository = new CartRepository();


        [HttpGet]
        [Route("carts")]
        public HttpResponseMessage GetAllCarts()
        {
            return Request.CreateResponse(HttpStatusCode.OK, cartRepository.GetAll().ToList());
        }


        [HttpGet]
        [Route("carts/{orderId}/{prodId}")]
        public HttpResponseMessage GetCart(int orderID, int prodId)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (orderID < 0)
            {
                shopErrors.Add(new ShopError(228));
            }
            if (prodId < 0)
            {
                shopErrors.Add(new ShopError(230));
            }
            if (orderID == 0 && prodId == 0)
            {
                shopErrors.Add(new ShopError(231));
            }
            if (shopErrors.Count > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                List<CartDTO> carts = cartRepository.Get(orderID, prodId).ToList();
                CartDTO singleCart;

                if (carts.Count > 0)
                {
                    if (carts.Count == 1)
                    {
                        singleCart = carts[0];
                        response = Request.CreateResponse(HttpStatusCode.OK, singleCart);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, carts);
                    }
                }
                else
                {
                    shopErrors.Add(new ShopError(210));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
            }

            return response;
        }


        [HttpPost]
        [Route("carts")]
        public HttpResponseMessage PostOrder([FromBody] Cart cart)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check cart for null
            if (cart == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.BadRequest, shopErrors);
            }
            else
            {
                if (cart.orderId <= 0)
                {
                    shopErrors.Add(new ShopError(228));
                }
                else if ((orderRepository.Get(cart.orderId)) == null)
                {
                    shopErrors.Add(new ShopError(228));
                }

                if (cart.prodId <= 0)
                {
                    shopErrors.Add(new ShopError(230));
                }
                else if ((productRepository.Get(cart.prodId)) == null)
                {
                    shopErrors.Add(new ShopError(230));
                }
                else if (cartRepository.Get(cart.orderId, cart.prodId).ToList().Count > 0)
                {
                    shopErrors.Add(new ShopError(237));
                }
                else if (!(bool)productRepository.Get(cart.prodId).inStock)
                {
                    shopErrors.Add(new ShopError(233));
                }

                if (cart.quantity <= 0)
                {
                    shopErrors.Add(new ShopError(232));

                }

                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        CartDTO cartDTO = cartRepository.Post(cart);
                        response = Request.CreateResponse(HttpStatusCode.Created, cartDTO);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }

            }

            return response;
        }

        [HttpPut]
        [Route("carts/{orderId}/{prodId}")]
        public HttpResponseMessage PutCart(int orderId, int prodId, [FromBody] Cart cart)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check order for null
            if (cart == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.BadRequest, shopErrors);
            }
            else
            {
                if (orderId <= 0)
                {
                    shopErrors.Add(new ShopError(228));
                }
                else if (cart.orderId != 0 && cart.orderId != orderId)
                {
                    shopErrors.Add(new ShopError(235));
                }
                else if ((orderRepository.Get(orderId)) == null)
                {
                    shopErrors.Add(new ShopError(228));
                }

                if (prodId <= 0)
                {
                    shopErrors.Add(new ShopError(230));
                }
                else if (cart.prodId != 0 && cart.prodId != prodId)
                {
                    shopErrors.Add(new ShopError(235));
                }
                else if ((productRepository.Get(prodId)) == null)
                {
                    shopErrors.Add(new ShopError(230));
                }
                else if (cartRepository.Get(orderId, prodId).ToList().Count <= 0)
                {
                    shopErrors.Add(new ShopError(236));
                }
                else if (!(bool)productRepository.Get(prodId).inStock)
                {
                    if (cartRepository.Get(orderId, prodId).ToList().First().quantity < cart.quantity)
                    {
                        shopErrors.Add(new ShopError(234));
                    }
                }

                if (cart.quantity <= 0)
                {
                    shopErrors.Add(new ShopError(232));
                }


                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        CartDTO cartDTO = cartRepository.Put(orderId, prodId, cart);
                        response = Request.CreateResponse(HttpStatusCode.Created, cartDTO);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }

            }

            return response;
        }

        [HttpDelete]
        [Route("carts/{orderId}/{prodId}")]
        public HttpResponseMessage DeleteCart(int orderId, int prodId)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (orderId <= 0)
            {
                shopErrors.Add(new ShopError(228));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else if (prodId <= 0)
            {
                shopErrors.Add(new ShopError(230));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                List<CartDTO> cart = cartRepository.Get(orderId, prodId).ToList();
                if (cart.Count == 0)
                {
                    shopErrors.Add(new ShopError(236));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        cartRepository.Delete(orderId, prodId);
                        response = Request.CreateResponse(HttpStatusCode.Accepted, "Cart Successfully Deleted");
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }
    }
}

﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using NBray_group_SOA_A04.Models;

namespace NBray_group_SOA_A04.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// This class provides the REST API calls for actions on the Customer
    /// </summary>
    [RoutePrefix("api")]
    public class CustomersController : ApiController
    {
        private static readonly CustomerRepository customerRepository = new CustomerRepository();

        /// <summary>
        /// Get a list of all customers in the database.
        /// </summary>
        /// <returns>HttpResponseMessage: A 200 status message with a list of Customers.
        /// It will return an empty List if no records are found.</returns>
        [HttpGet]
        [Route("customers")]
        public HttpResponseMessage GetAllCustomers()
        {
            return Request.CreateResponse(HttpStatusCode.OK, customerRepository.GetAll());
        }


        /// <summary>
        /// Get the Customer given the ID found in the URL.
        /// </summary>
        /// <param name="id">The ID of the customer.</param>
        /// <returns>HttpResponseMessage: If an error occurs while attempting to find the Customer,
        /// it will return that. Otherwise, it will return the resulting Customer.</returns>
        [HttpGet]
        [Route("customers/{id}")]
        public HttpResponseMessage GetCustomer(int id)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            // Valid int above 0
            if (id <= 0)
            {
                shopErrors.Add(new ShopError(212));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                CustomerDTO customer = customerRepository.Get(id);
                // Customer not found
                if (customer == null)
                {
                    shopErrors.Add(new ShopError(210));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    // Happy path return
                    response = Request.CreateResponse(HttpStatusCode.OK, customer);
                }
            }

            return response;
        }


        /// <summary>
        /// Get Customer information given any combination of First Name, Last Name, and Phone Number.
        /// </summary>
        /// <param name="firstName">The First Name of the customer or 0 if not used</param>
        /// <param name="lastName">The Last Name of the customer or 0 if not used</param>
        /// <param name="phoneNumber">The Phone Number of the customer or 0 if not used</param>
        /// <returns>HttpResponseMessage: If an error occurs while attempting to find the Customer,
        /// it will return that. Otherwise, it will return a list of results.</returns>
        [HttpGet]
        [Route("customers/{firstName}/{lastName}/{phoneNumber}")]
        public HttpResponseMessage GetCustomer(string firstName, string lastName, string phoneNumber)
        {
            HttpResponseMessage response;
            List<ShopError> shopErrors = new List<ShopError>();

            // If it's 0, set it to null for the query
            if (firstName == "0")
            {
                firstName = null;
            }
            else
            {
                // If it isn't null, validate
                ShopError err;
                if ((err = Customer.ValidateFirstNameField(firstName)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            // If it's 0, set it to null for the query
            if (lastName == "0")
            {
                lastName = null;
            }
            else
            {
                // If it isn't null, validate
                ShopError err;
                if ((err = Customer.ValidateLastNameField(lastName)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            // If it's 0, set it to null for the query
            if (phoneNumber == "0")
            {
                phoneNumber = null;
            }
            else
            {
                // If it isn't null, validate
                ShopError err;
                if ((err = Customer.ValidatePhoneNumber(phoneNumber)) != null)
                {
                    shopErrors.Add(err);
                }

                // For cases where they send a phone number without dashes, insert them to keep data consistent
                if (Regex.IsMatch(phoneNumber, @"^\d{10}$"))
                {
                    phoneNumber = phoneNumber.Insert(6, "-");
                    phoneNumber = phoneNumber.Insert(3, "-");
                }
            }

            // If errors
            if (shopErrors.Count > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                try
                {
                    List<CustomerDTO> customerList = customerRepository.Get(firstName, lastName, phoneNumber).ToList();
                    if (customerList.Count == 0)
                    {
                        // No results
                        shopErrors.Add(new ShopError(210));
                        response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                    }
                    else
                    {
                        // Happy Return
                        response = Request.CreateResponse(HttpStatusCode.OK, customerList);
                    }
                }
                catch (Exception ex)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                }
            }

            return response;
        }


        /// <summary>
        /// Add a Customer to the system
        /// </summary>
        /// <param name="customer">The Customer to add</param>
        /// <returns>HttpResponseMessage: If an error occurs while attempting to Insert the Customer,
        /// it will return that. Otherwise, it will return the successfully inserted Customer.</returns>
        [HttpPost]
        [Route("customers")]
        public HttpResponseMessage PostCustomer([FromBody] Customer customer)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check customer for null
            if (customer == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.BadRequest, shopErrors);
            }
            else
            {
                // Check if the body contains custId
                if (customer.custId != 0)
                {
                    shopErrors.Add(new ShopError(238));
                }

                // Validate Fields
                ShopError err;
                if ((err = Customer.ValidateFirstNameField(customer.firstName)) != null)
                {
                    shopErrors.Add(err);
                }

                if ((err = Customer.ValidateLastNameField(customer.lastName)) != null)
                {
                    shopErrors.Add(err);
                }

                if ((err = Customer.ValidatePhoneNumber(customer.phoneNumber)) != null)
                {
                    shopErrors.Add(err);
                }
                else if (Regex.IsMatch(customer.phoneNumber, @"^\d{10}$"))
                {
                    // If the phone number is just numbers, insert the dashes
                    customer.phoneNumber = customer.phoneNumber.Insert(6, "-");
                    customer.phoneNumber = customer.phoneNumber.Insert(3, "-");
                }

                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        // Try to insert, and then send the inserted Customer in the response
                        CustomerDTO newCustomer = customerRepository.Post(customer);
                        response = Request.CreateResponse(HttpStatusCode.Created, newCustomer);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }


        /// <summary>
        /// Update a customer given the id in the request
        /// </summary>
        /// <param name="id">The id of the Customer to update</param>
        /// <param name="customer">The Customer object containing the updated information</param>
        /// <returns>HttpResponseMessage: If an error occurs while attempting to Update the Customer,
        /// it will return that. Otherwise, it will return the successfully updated Customer.</returns>
        [HttpPut]
        [Route("customers/{id}")]
        public HttpResponseMessage PutCustomer(int id, [FromBody] CustomerDTO customer)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check customer for null
            if (customer == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                Customer finalCustomer = new Customer();
                CustomerDTO currentCustomerDto;
                // Validate the Id
                if (id <= 0)
                {
                    shopErrors.Add(new ShopError(212));
                }
                else if (customer.custId != 0 && customer.custId != id)
                {
                    shopErrors.Add(new ShopError(215));
                }
                else if ((currentCustomerDto = customerRepository.Get(id)) == null)
                {
                    shopErrors.Add(new ShopError(212));
                }
                else
                {
                    // Transfer the new information to a new object.
                    // Any field we don't have update info for, we use the old record to populate with
                    finalCustomer.custId = id;
                    finalCustomer.firstName = customer.firstName ?? currentCustomerDto.firstName;
                    finalCustomer.lastName = customer.lastName ?? currentCustomerDto.lastName;
                    finalCustomer.phoneNumber = customer.phoneNumber ?? currentCustomerDto.phoneNumber;

                    // Validate the fields
                    ShopError err;
                    if ((err = Customer.ValidateFirstNameField(finalCustomer.firstName)) != null)
                    {
                        shopErrors.Add(err);
                    }

                    if ((err = Customer.ValidateLastNameField(finalCustomer.lastName)) != null)
                    {
                        shopErrors.Add(err);
                    }

                    if ((err = Customer.ValidatePhoneNumber(finalCustomer.phoneNumber)) != null)
                    {
                        shopErrors.Add(err);
                    }
                    else if (Regex.IsMatch(finalCustomer.phoneNumber, @"^\d{10}$"))
                    {
                        customer.phoneNumber = finalCustomer.phoneNumber.Insert(6, "-");
                        customer.phoneNumber = finalCustomer.phoneNumber.Insert(3, "-");
                    }
                }

                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        CustomerDTO updatedCustomer = customerRepository.Put(id, finalCustomer);
                        response = Request.CreateResponse(HttpStatusCode.Accepted, updatedCustomer);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">The id of the Customer to Delete</param>
        /// <returns>HttpResponseMessage: If an error occurs while attempting to Delete the Customer,
        /// it will return that. Otherwise, it will return a success message.</returns>
        [HttpDelete]
        [Route("customers/{id}")]
        public HttpResponseMessage DeleteCustomer(int id)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (id <= 0)
            {
                shopErrors.Add(new ShopError(212));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                CustomerDTO customer = customerRepository.Get(id);
                if (customer == null)
                {
                    shopErrors.Add(new ShopError(212));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        customerRepository.Delete(id);
                        response = Request.CreateResponse(HttpStatusCode.Accepted, "Customer Successfully Deleted");
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }
    }
}
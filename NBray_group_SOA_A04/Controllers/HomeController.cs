﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

using System.Web.Mvc;

namespace NBray_group_SOA_A04.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.productsAPI = Url.Content("~/api/products/");
            ViewBag.customersAPI = Url.Content("~/api/customers/");
            return View();
        }
    }
}

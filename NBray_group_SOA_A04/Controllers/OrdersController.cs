﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NBray_group_SOA_A04.Models;
using System.Text.RegularExpressions;

namespace NBray_group_SOA_A04.Controllers
{
    [RoutePrefix("api")]
    public class OrdersController : ApiController
    {
        private static readonly CustomerRepository customerRepository = new CustomerRepository();
        private static readonly OrderRepository orderRepository = new OrderRepository();

        [HttpGet]
        [Route("orders")]
        public HttpResponseMessage GetAllOrders()
        {
            return Request.CreateResponse(HttpStatusCode.OK, orderRepository.GetAll());
        }


        [HttpGet]
        [Route("orders/{id}")]
        public HttpResponseMessage GetOrder(int id)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (id <= 0)
            {
                shopErrors.Add(new ShopError(212));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                OrderDTO order = orderRepository.Get(id);
                if (order == null)
                {
                    shopErrors.Add(new ShopError(210));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, order);
                }
            }

            return response;
        }

        [HttpGet]
        [Route("orders/{customerId}/{orderDate}/{poNumber}")]
        public HttpResponseMessage GetOrder(int customerId, string orderDate, string poNumber)
        {
            HttpResponseMessage response;
            List<ShopError> shopErrors = new List<ShopError>();
            if (customerId != 0)
            {
                if ((customerRepository.Get(customerId)) == null)
                {
                    shopErrors.Add(new ShopError(226));
                }
            }

            DateTime dt = DateTime.MinValue;
            if (orderDate != "0")
            {
                if (DateTime.TryParseExact(orderDate, "MM-dd-yy", new CultureInfo("en-US"), DateTimeStyles.None, out dt))
                {
                    ShopError err;
                    if ((err = Order.ValidateDate(dt)) != null)
                    {
                        shopErrors.Add(err);
                    }
                }
                else
                {
                    shopErrors.Add(new ShopError(225));
                }
                
            }

            if (poNumber == "0")
            {
                poNumber = null;
            }
            else
            {
                ShopError err;
                if ((err = Order.ValidatePoNumber(poNumber)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            if (shopErrors.Count > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                try
                {
                    List<OrderDTO> orderList = orderRepository.Get(customerId, dt, poNumber).ToList();
                    if (orderList.Count == 0)
                    {
                        shopErrors.Add(new ShopError(210));
                        response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, orderList);
                    }
                }
                catch (Exception ex)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                }
            }

            return response; //customerRepository.Get(firstName, lastName, phoneNumber);
        }

        [HttpPost]
        [Route("orders")]
        public HttpResponseMessage PostOrder([FromBody] Order order)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check order for null
            if (order == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.BadRequest, shopErrors);
            }
            else
            {
                if (order.orderId != 0)
                {
                    shopErrors.Add(new ShopError(209));
                }

                if (order.custId <= 0)
                {
                    shopErrors.Add(new ShopError(226));
                }
                else if ((customerRepository.Get(order.custId)) == null)
                {
                    shopErrors.Add(new ShopError(226));
                }

                ShopError err;
                if ((err = Order.ValidateDate(order.orderDate)) != null)
                {
                    shopErrors.Add(err);
                }

                if ((err = Order.ValidatePoNumber(order.poNumber)) != null)
                {
                    shopErrors.Add(err);
                }

                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        OrderDTO orderDto = orderRepository.Post(order);
                        response = Request.CreateResponse(HttpStatusCode.Created, orderDto);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }

            }

            return response;
        }

        [HttpPut]
        [Route("orders/{id}")]
        public HttpResponseMessage PutOrder(int id, [FromBody] OrderDTO order)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check order for null
            if (order == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.BadRequest, shopErrors);
            }
            else
            {
                Order finalOrder = new Order();
                OrderDTO currentOrderDto;
                if (id <= 0)
                {
                    shopErrors.Add(new ShopError(228));
                }
                else if (order.orderId != 0 && order.orderId != id)
                {
                    shopErrors.Add(new ShopError(229));
                }
                else if ((currentOrderDto = orderRepository.Get(id)) == null)
                {
                    shopErrors.Add(new ShopError(228));
                }
                else
                {
                    finalOrder.orderId = id;
                    finalOrder.orderDate = (DateTime)(order.orderDate ?? currentOrderDto.orderDate);
                    finalOrder.poNumber = order.poNumber ?? currentOrderDto.poNumber;
                    finalOrder.custId = (int)(order.custId ?? currentOrderDto.custId);

                    if (order.custId <= 0)
                    {
                        shopErrors.Add(new ShopError(226));
                    }
                    else if (customerRepository.Get(finalOrder.custId) == null)
                    {
                        shopErrors.Add(new ShopError(226));
                    }

                    ShopError err;
                    if ((err = Order.ValidateDate(finalOrder.orderDate)) != null)
                    {
                        shopErrors.Add(err);
                    }

                    if ((err = Order.ValidatePoNumber(finalOrder.poNumber)) != null)
                    {
                        shopErrors.Add(err);
                    }
                }
                
                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        OrderDTO orderDto = orderRepository.Put(id, finalOrder);
                        response = Request.CreateResponse(HttpStatusCode.Created, orderDto);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }

            }

            return response;
        }

        [HttpDelete]
        [Route("orders/{id}")]
        public HttpResponseMessage DeleteOrder(int id)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (id <= 0)
            {
                shopErrors.Add(new ShopError(212));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                OrderDTO order = orderRepository.Get(id);
                if (order == null)
                {
                    shopErrors.Add(new ShopError(212));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        orderRepository.Delete(id);
                        response = Request.CreateResponse(HttpStatusCode.Accepted, "Order Successfully Deleted");
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }

        [HttpGet]
        [Route("custOrders/{custId}/{poNumber}/{orderDate}/{firstName}/{lastName}/{phoneNumber}")]
        public HttpResponseMessage GetCustOrders(int custId, string poNumber, string orderDate, string firstName, string lastName, string phoneNumber)
        {
            HttpResponseMessage response;
            List<ShopError> shopErrors = new List<ShopError>();
            if (custId != 0)
            {
                if ((customerRepository.Get(custId)) == null)
                {
                    shopErrors.Add(new ShopError(226));
                }
            }

            DateTime dt = DateTime.MinValue;
            if (orderDate != "0")
            {
                if (DateTime.TryParseExact(orderDate, "MM-dd-yy", new CultureInfo("en-US"), DateTimeStyles.None, out dt))
                {
                    ShopError err;
                    if ((err = Order.ValidateDate(dt)) != null)
                    {
                        shopErrors.Add(err);
                    }
                }
                else
                {
                    shopErrors.Add(new ShopError(225));
                }
            }

            if (poNumber == "0")
            {
                poNumber = null;
            }
            else
            {
                ShopError err;
                if ((err = Order.ValidatePoNumber(poNumber)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            if (firstName == "0")
            {
                firstName = null;
            }
            else
            {
                ShopError err;
                if ((err = Customer.ValidateFirstNameField(firstName)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            if (lastName == "0")
            {
                lastName = null;
            }
            else
            {
                ShopError err;
                if ((err = Customer.ValidateLastNameField(lastName)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            if (phoneNumber == "0")
            {
                phoneNumber = null;
            }
            else
            {
                ShopError err;
                if ((err = Customer.ValidatePhoneNumber(phoneNumber)) != null)
                {
                    shopErrors.Add(err);
                }

                if (Regex.IsMatch(phoneNumber, @"^\d{10}$"))
                {
                    phoneNumber = phoneNumber.Insert(6, "-");
                    phoneNumber = phoneNumber.Insert(3, "-");
                }
            }

            if (shopErrors.Count > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                try
                {
                    List<CustOrderDTO> orderList = orderRepository.Get(custId, poNumber, dt, firstName, lastName, phoneNumber).ToList();
                    if (orderList.Count == 0)
                    {
                        shopErrors.Add(new ShopError(210));
                        response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, orderList);
                    }
                }
                catch (Exception ex)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                }
            }

            return response;
        }
    }
}

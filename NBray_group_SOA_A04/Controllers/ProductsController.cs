﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NBray_group_SOA_A04.Models;

namespace NBray_group_SOA_A04.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [RoutePrefix("api")]
    public class ProductsController : ApiController
    {
        private static readonly ProductRepository productRepository = new ProductRepository();

        /// <summary>
        /// This is the API call to get a list of all Products
        /// </summary>
        /// <returns>HttpResponseMessage containing a list of all products</returns>
        [HttpGet]
        [Route("products")]
        public HttpResponseMessage GetAllProducts()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, productRepository.GetAll());

            return response;
        }

        [HttpGet]
        [Route("products/{id:int}")]
        public HttpResponseMessage GetProduct(int id)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (id <= 0)
            {
                shopErrors.Add(new ShopError(212));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                ProductDTO product = productRepository.Get(id);
                if (product == null)
                {
                    shopErrors.Add(new ShopError(210));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, product);
                }
            }

            return response;
        }

        [HttpGet]
        [Route("products/{prodName}")]
        public HttpResponseMessage GetProduct(string prodName)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            ShopError err;
            if ((err = Product.ValidateProductName(new Product {prodName = prodName})) != null)
            {
                shopErrors.Add(err);
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                string input = prodName.Trim().ToLower();
                List<ProductDTO> products;
                switch (input)
                {
                    case "yes":
                    case "true":
                        products = productRepository.Get(true).ToList();
                        break;
                    case "no":
                    case "false":
                        products = productRepository.Get(false).ToList();
                        break;
                    default:
                        products = productRepository.Get(prodName).ToList();
                        break;
                }

                if (products.Count == 0)
                {
                    shopErrors.Add(new ShopError(210));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, products);
                }
            }

            return response;
        }

        [HttpPost]
        [Route("products")]
        public HttpResponseMessage PostProduct([FromBody] Product product)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check p for null
            if (product == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.BadRequest, shopErrors);
            }
            else
            {
                if (product.prodID != 0)
                {
                    shopErrors.Add(new ShopError(209));
                }

                ShopError err;
                if ((err = Product.ValidateProductName(product)) != null)
                {
                    shopErrors.Add(err);
                }
                else
                {
                    string prodName = product.prodName.ToLower();
                    switch (prodName)
                    {
                        case "yes":
                        case "true":
                        case "no":
                        case "false":
                            shopErrors.Add(new ShopError(211));
                            break;
                    }
                }

                List<ShopError> errList;
                if ((errList = Product.ValidateFloats(product)).Count > 0)
                {
                    shopErrors.AddRange(errList);
                }


                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        ProductDTO newProduct = productRepository.Post(product);
                        response = Request.CreateResponse(HttpStatusCode.Created, newProduct);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }


        [HttpPut]
        [Route("products/{id}")]
        public HttpResponseMessage PutProduct(int id, [FromBody] ProductDTO product)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;
            // Check p for null
            if (product == null)
            {
                shopErrors.Add(new ShopError(204));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                Product p = new Product();
                ProductDTO pro;

                if (id <= 0)
                {
                    shopErrors.Add(new ShopError(212));
                }
                else if (product.prodID != 0 && product.prodID != id)
                {
                    shopErrors.Add(new ShopError(213));
                }
                else if ((pro = productRepository.Get(id)) == null)
                {
                    shopErrors.Add(new ShopError(212));
                }
                else
                {

                    p.prodID = id;
                    p.prodName = product.prodName ?? pro.prodName;
                    p.price = (float) (product.price ?? pro.price);
                    p.prodWeight = (float) (product.prodWeight ?? pro.prodWeight);
                    p.inStock = (bool) (product.inStock ?? pro.inStock);

                    ShopError err;
                    if ((err = Product.ValidateProductName(p)) != null)
                    {
                        shopErrors.Add(err);
                    }

                    string prodName = p.prodName.ToLower();
                    switch (prodName)
                    {
                        case "yes":
                        case "true":
                        case "no":
                        case "false":
                            shopErrors.Add(new ShopError(211));
                            break;
                    }

                    List<ShopError> errList;
                    if ((errList = Product.ValidateFloats(p)).Count > 0)
                    {
                        shopErrors.AddRange(errList);
                    }
                }

                if (shopErrors.Count > 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        ProductDTO updatedProduct = productRepository.Put(id, p);
                        response = Request.CreateResponse(HttpStatusCode.Accepted, updatedProduct);
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }

        [HttpDelete]
        [Route("products/{id}")]
        public HttpResponseMessage DeleteProduct(int id)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (id <= 0)
            {
                shopErrors.Add(new ShopError(212));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                ProductDTO product = productRepository.Get(id);
                if (product == null)
                {
                    shopErrors.Add(new ShopError(212));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    try
                    {
                        productRepository.Delete(id);
                        response = Request.CreateResponse(HttpStatusCode.Accepted, "Product Successfully Deleted");
                    }
                    catch (Exception ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    }
                }
            }

            return response;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NBray_group_SOA_A04.Models;

namespace NBray_group_SOA_A04.Controllers
{
    [RoutePrefix("api")]
    public class PurchaseOrderController: ApiController
    {
        private static readonly PurchaseOrderRepository poRepository = new PurchaseOrderRepository();
        private static readonly OrderRepository orderRepository = new OrderRepository();
        private static readonly CustomerRepository customerRepository = new CustomerRepository();


        [HttpGet]
        [Route("purchaseOrders/{orderId}/{custId}/{firstName}/{lastName}/{poNumber}/{orderDate}/")]
        public HttpResponseMessage GetPurchaseOrder(int orderId, int custId, string firstName, string lastName, string poNumber, string orderDate)
        {
            HttpResponseMessage response;
            List<ShopError> shopErrors = new List<ShopError>();
            if (orderId != 0)
            {
                if ((orderRepository.Get(orderId)) == null)
                {
                    shopErrors.Add(new ShopError(228));
                }
            }

            if (custId != 0)
            {
                if ((customerRepository.Get(custId)) == null)
                {
                    shopErrors.Add(new ShopError(226));
                }
            }

            DateTime dt = DateTime.MinValue;
            if (orderDate != "0")
            {
                if (DateTime.TryParseExact(orderDate, "MM-dd-yy", new CultureInfo("en-US"), DateTimeStyles.None, out dt))
                {
                    ShopError err;
                    if ((err = Order.ValidateDate(dt)) != null)
                    {
                        shopErrors.Add(err);
                    }
                }
                else
                {
                    shopErrors.Add(new ShopError(225));
                }
            }

            if (poNumber == "0")
            {
                poNumber = null;
            }
            else
            {
                ShopError err;
                if ((err = Order.ValidatePoNumber(poNumber)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            if (firstName == "0")
            {
                firstName = null;
            }
            else
            {
                ShopError err;
                if ((err = Customer.ValidateFirstNameField(firstName)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            if (lastName == "0")
            {
                lastName = null;
            }
            else
            {
                ShopError err;
                if ((err = Customer.ValidateLastNameField(lastName)) != null)
                {
                    shopErrors.Add(err);
                }
            }

            if (shopErrors.Count > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                try
                {
                    List<HeaderDTO> orderList = poRepository.Get(orderId, custId, firstName, lastName, poNumber, dt).ToList();
                    if (orderList.Count == 0)
                    {
                        shopErrors.Add(new ShopError(210));
                        response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, orderList);
                    }
                }
                catch (Exception ex)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                }
            }

            return response;
        }

        [HttpGet]
        [Route("purchaseOrders/{orderId}")]
        public HttpResponseMessage GetPurchaseOrder(int orderId)
        {
            List<ShopError> shopErrors = new List<ShopError>();
            HttpResponseMessage response;

            if (orderId <= 0)
            {
                shopErrors.Add(new ShopError(228));
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
            }
            else
            {
                if (orderRepository.Get(orderId) == null)
                {
                    shopErrors.Add(new ShopError(228));
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                }
                else
                {
                    PurchaseOrderDTO po = poRepository.Get(orderId);
                    if (po == null)
                    {
                        shopErrors.Add(new ShopError(210));
                        response = Request.CreateResponse(HttpStatusCode.NotAcceptable, shopErrors);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, po);
                    }
                }
            }

            return response;
        }
    }
}
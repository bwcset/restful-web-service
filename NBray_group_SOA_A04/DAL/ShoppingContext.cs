﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using NBray_group_SOA_A04.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace NBray_group_SOA_A04.DAL
{
    /// <inheritdoc />
    /// <summary>
    /// This class is to set up the context/connection to the database we are going to use
    /// </summary>
    public class ShoppingContext : DbContext
    {
        /// <inheritdoc />
        /// <summary>
        /// Uses the contructor to create a context/ connection given the name of the connection string found
        /// in web.config
        /// </summary>
        public ShoppingContext() : base("connString")
        { }

        /// <summary>
        /// Collection of all Customer entities
        /// </summary>
        public DbSet<Customer> Customers { get; set; }
        /// <summary>
        /// Collection of all Product entities
        /// </summary>
        public DbSet<Product> Products { get; set; }
        /// <summary>
        /// Collection of all Order entities
        /// </summary>
        public DbSet<Order> Orders { get; set; }
        /// <summary>
        /// Collection of all Cart entities
        /// </summary>
        public DbSet<Cart> Carts { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// This override is used to set up the foreign key constraints for the database EF makes
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Cart>().HasKey(t => new { t.orderId, t.prodId });

            modelBuilder.Entity<Order>()
              .HasRequired(o => o.Customer)
              .WithMany(c => c.Orders)
              .HasForeignKey(o => o.custId);

            modelBuilder.Entity<Cart>()
              .HasRequired(c => c.Order)
              .WithMany(o => o.Carts)
              .HasForeignKey(c => c.orderId);

            modelBuilder.Entity<Cart>()
              .HasRequired(c => c.Product)
              .WithMany(p => p.Carts)
              .HasForeignKey(c => c.prodId);
        }
    }
}
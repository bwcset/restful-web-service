﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System;
using System.Collections.Generic;
using System.Data.Entity;
using NBray_group_SOA_A04.Models;

namespace NBray_group_SOA_A04.DAL
{
    /// <inheritdoc />
    /// <summary>
    /// This class is to set up the sample data once the database is created
    /// </summary>
    public class ShoppingInit : DropCreateDatabaseIfModelChanges<ShoppingContext>
    {
        /// <inheritdoc />
        /// <summary>
        /// This override is to customize out seed data
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(ShoppingContext context)
        {
            List<Customer> customers = new List<Customer>
            {
                new Customer{firstName="Joe",lastName="Smith", phoneNumber="555-555-1212"},
                new Customer{firstName="Nancy",lastName="Jones", phoneNumber="555-235-4578"},
                new Customer{firstName="Henry",lastName="Hoover", phoneNumber="555-326-8456"}
            };

            customers.ForEach(s => context.Customers.Add(s));
            context.SaveChanges();

            List<Product> products = new List<Product>
            {
                new Product{prodName="Grommet", price=0.02f, prodWeight=0.005f, inStock=true},
                new Product{prodName="Widgets", price=2.35f, prodWeight=0.532f, inStock=true},
                new Product{prodName="Bushings", price=8.75f, prodWeight=5.650f, inStock=false},
            };

            products.ForEach(s => context.Products.Add(s));
            context.SaveChanges();

            List<Order> orders = new List<Order>
            {
                new Order{custId=1, orderDate=DateTime.Parse("2018-09-15"), poNumber="GRAP-09-2018-001"},
                new Order{custId=1, orderDate=DateTime.Parse("2018-09-30"), poNumber="GRAP-09-2018-056"},
                new Order{custId=3, orderDate=DateTime.Parse("2018-10-05"), poNumber=""},
            };

            orders.ForEach(s => context.Orders.Add(s));
            context.SaveChanges();


            List<Cart> carts = new List<Cart>
            {
                new Cart{orderId=1, prodId=1, quantity=500},
                new Cart{orderId=1, prodId=2, quantity=1000},
                new Cart{orderId=2, prodId=3, quantity=10},
                new Cart{orderId=3, prodId=1, quantity=75},
                new Cart{orderId=3, prodId=2, quantity=15},
                new Cart{orderId=3, prodId=3, quantity=5},

            };

            carts.ForEach(s => context.Carts.Add(s));
            context.SaveChanges();
        }
    }
}
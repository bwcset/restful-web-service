﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

using System;
using System.Linq;
using NBray_group_SOA_A04.DAL;

namespace NBray_group_SOA_A04.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CartRepository
    {
        private ShoppingContext db = new ShoppingContext();

        internal IQueryable<CartDTO> GetAll()
        {
            IQueryable<CartDTO> carts = from c in db.Carts
                select new CartDTO()
                {
                    orderId = c.orderId,
                    prodId = c.prodId,
                    quantity = (c.Product.inStock ? c.quantity : 0)
                };

            return carts;
        }


        public IQueryable<CartDTO> Get(int orderId, int prodId)
        {
            IQueryable<CartDTO> cart = from c in db.Carts
                    .Where(p => (orderId == 0 || p.orderId == orderId) &&
                                (prodId == 0 || p.prodId == prodId))
                select new CartDTO()
                {
                    orderId = c.orderId,
                    prodId = c.prodId,
                    quantity = c.quantity
                };

            return cart;
        }


        public CartDTO Post(Cart cart)
        {
            db.Carts.Add(cart);
            db.SaveChanges();

            CartDTO dto = new CartDTO()
            {
                orderId = cart.orderId,
                prodId = cart.prodId,
                quantity = cart.quantity
            };

            return dto;
        }

        public CartDTO Put(int orderId, int prodId, Cart cart)
        {
            Cart origCart = db.Carts.FindAsync(orderId, prodId).Result;
            if (origCart == null) return null;
            cart.orderId = orderId;
            cart.prodId = prodId;
            db.Entry(origCart).CurrentValues.SetValues(cart);
            db.SaveChanges();

            CartDTO dto = new CartDTO()
            {
                orderId = cart.orderId,
                prodId = cart.prodId,
                quantity = cart.quantity
            };

            return dto;
        }

        public void Delete(int orderId, int prodId)
        {
            db.Carts.Remove(db.Carts.FindAsync(orderId, prodId).Result ?? throw new InvalidOperationException());
            db.SaveChanges();
        }
    }
}
﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System;
using System.Linq;
using NBray_group_SOA_A04.DAL;

namespace NBray_group_SOA_A04.Models
{
    /// <summary>
    /// This class is provides the access to the database using Linq to make the queries
    /// </summary>
    public class CustomerRepository
    {
        private ShoppingContext db = new ShoppingContext();

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        internal IQueryable<CustomerDTO> GetAll()
        {
            IQueryable<CustomerDTO> customers = from c in db.Customers
                select new CustomerDTO()
                {
                    custId = c.custId,
                    firstName = c.firstName,
                    lastName = c.lastName,
                    phoneNumber = c.phoneNumber
                };

            return customers;
        }


        /// <summary>
        /// get Customer using the firstName, lastName, phoneNumber
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public IQueryable<CustomerDTO> Get(string firstName, string lastName, string phoneNumber)
        {
            //Use DAL to query for the correct Customers
            IQueryable<CustomerDTO> customers = from c in db.Customers
                    .Where(p => (firstName == null || p.firstName == firstName) &&
                                (lastName == null || p.lastName == lastName) &&
                                (phoneNumber == null || p.phoneNumber == phoneNumber))
                select new CustomerDTO()
                {
                    custId = c.custId,
                    firstName = c.firstName,
                    lastName = c.lastName,
                    phoneNumber = c.phoneNumber
                };


            return customers;
        }


        /// <summary>
        /// get customer by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomerDTO Get(int id)
        {
            CustomerDTO customer = db.Customers.Select(c =>
                new CustomerDTO()
                {
                    custId = c.custId,
                    firstName = c.firstName,
                    lastName = c.lastName,
                    phoneNumber = c.phoneNumber
                }).SingleOrDefault(c => c.custId == id);

            return customer;
        }


        /// <summary>
        /// Insert the customer
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public CustomerDTO Post(Customer c)
        {
            if (c.firstName != null)
            {
                c.firstName = c.firstName.Trim();
            }

            c.lastName = c.lastName.Trim();
            db.Customers.Add(c);
            db.SaveChanges();

            CustomerDTO dto = new CustomerDTO()
            {
                custId = c.custId,
                firstName = c.firstName,
                lastName = c.lastName,
                phoneNumber = c.phoneNumber
            };

            return dto;
        }


        /// <summary>
        /// Update the customer with the id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public CustomerDTO Put(int id, Customer c)
        {
            Customer customer = db.Customers.FindAsync(id).Result;
            if (customer == null) return null;
            c.custId = id;
            db.Entry(customer).CurrentValues.SetValues(c);
            db.SaveChanges();

            CustomerDTO dto = new CustomerDTO()
            {
                custId = c.custId,
                firstName = c.firstName,
                lastName = c.lastName,
                phoneNumber = c.phoneNumber
            };

            return dto;
        }


        /// <summary>
        /// delete Customer by the given id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            db.Customers.Remove(db.Customers.FindAsync(id).Result ?? throw new InvalidOperationException());
            db.SaveChanges();
        }
    }
}
﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using NBray_group_SOA_A04.DAL;

namespace NBray_group_SOA_A04.Models
{
    public class OrderRepository
    {
        private ShoppingContext db = new ShoppingContext();

        internal IQueryable<OrderDTO> GetAll()
        {
            IQueryable<OrderDTO> orders = from order in db.Orders
                select new OrderDTO()
                {
                    orderId = order.orderId,
                    custId = order.custId,
                    poNumber = order.poNumber,
                    orderDate = order.orderDate,
                };

            return orders;
        }


        public OrderDTO Get(int id)
        {
            OrderDTO order = db.Orders.Select(o =>
                new OrderDTO()
                {
                    orderId = o.orderId,
                    custId = o.custId,
                    poNumber = o.poNumber,
                    orderDate = o.orderDate,
                }).SingleOrDefault(o => o.orderId == id);

            return order;
        }

        public IQueryable<OrderDTO> Get(int custId, DateTime orderDate, string poNumber)
        {
            //Use DAL to query for the correct Orders
            IQueryable<OrderDTO> order = from o in db.Orders
                    .Where(p => (custId == 0 || p.custId == custId) &&
                                (orderDate == DateTime.MinValue || p.orderDate == orderDate) &&
                                (poNumber == null || p.poNumber == poNumber))
                select new OrderDTO()
                {
                    orderId = o.orderId,
                    custId = o.custId,
                    poNumber = o.poNumber,
                    orderDate = o.orderDate,
                };


            return order;
        }

        public OrderDTO Post(Order order)
        {
            if (order.poNumber != null)
            {
                order.poNumber = order.poNumber.Trim();
            }

            db.Orders.Add(order);
            db.SaveChanges();

            OrderDTO dto = new OrderDTO()
            {
                orderId = order.orderId,
                custId = order.custId,
                poNumber = order.poNumber,
                orderDate = order.orderDate,
            };
            return dto;
        }

        public OrderDTO Put(int id, Order order)
        {
            Order newOrder = db.Orders.FindAsync(id).Result;
            if (newOrder == null) return null;
            order.orderId = id;
            db.Entry(newOrder).CurrentValues.SetValues(order);
            db.SaveChanges();

            OrderDTO dto = new OrderDTO()
            {
                orderId = order.orderId,
                custId = order.custId,
                poNumber = order.poNumber,
                orderDate = order.orderDate,
            };

            return dto;
        }

        public void Delete(int id)
        {
            db.Orders.Remove(db.Orders.FindAsync(id).Result ?? throw new InvalidOperationException());
            db.SaveChanges();
        }

        public IQueryable<CustOrderDTO> Get(int custId, string poNumber, DateTime orderDate, string firstName,
            string lastName, string phoneNumber)
        {
            //Use DAL to query for the correct Orders
            IQueryable<CustOrderDTO> custOrder = from o in db.Orders
                    .Where(p => (custId == 0 || p.custId == custId) &&
                                (orderDate == DateTime.MinValue || p.orderDate == orderDate) &&
                                (poNumber == null || p.poNumber == poNumber) &&
                                (firstName == null || p.Customer.firstName == firstName) &&
                                (lastName == null || p.Customer.lastName == lastName) &&
                                (phoneNumber == null || p.Customer.phoneNumber == phoneNumber))
                select new CustOrderDTO()
                {
                    orderId = o.orderId,
                    custId = o.custId,
                    poNumber = o.poNumber,
                    orderDate = DbFunctions.Right("0" + SqlFunctions.DatePart("month", o.orderDate), 2) + "-"  + DbFunctions.Right("0" + SqlFunctions.DatePart("day", o.orderDate), 2) + "-" + DbFunctions.Right("" + SqlFunctions.DatePart("year", o.orderDate), 2),
                    firstName = o.Customer.firstName,
                    lastName = o.Customer.lastName,
                    phoneNumber = o.Customer.phoneNumber
                };


            return custOrder;
        }
    }
}
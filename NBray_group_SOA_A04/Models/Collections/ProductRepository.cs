﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System.Linq;
using NBray_group_SOA_A04.DAL;

namespace NBray_group_SOA_A04.Models
{
    public class ProductRepository
    {
        private ShoppingContext db = new ShoppingContext();

        // Get the product list
        public IQueryable<ProductDTO> GetAll()
        {
            IQueryable<ProductDTO> products = from pro in db.Products
                select new ProductDTO()
                {
                    prodID = pro.prodID,
                    prodName = pro.prodName,
                    inStock = pro.inStock,
                    price = pro.price,
                    prodWeight = pro.prodWeight
                };

            return products;
        }

        public ProductDTO Get(int id)
        {
            ProductDTO product = db.Products.Select(pro =>
                new ProductDTO()
                {
                    prodID = pro.prodID,
                    prodName = pro.prodName,
                    inStock = pro.inStock,
                    price = pro.price,
                    prodWeight = pro.prodWeight
                }).SingleOrDefault(pro => pro.prodID == id);

            return product;
        }

        public IQueryable<ProductDTO> Get(string prodName)
        {
            IQueryable<ProductDTO> products = from pro in db.Products
                    .Where(p => p.prodName == prodName)
                select new ProductDTO()
                {
                    prodID = pro.prodID,
                    prodName = pro.prodName,
                    inStock = pro.inStock,
                    price = pro.price,
                    prodWeight = pro.prodWeight
                };

            return products;
        }

        public IQueryable<ProductDTO> Get(bool inStock)
        {
            IQueryable<ProductDTO> products = from pro in db.Products
                    .Where(p => p.inStock == inStock)
                select new ProductDTO()
                {
                    prodID = pro.prodID,
                    prodName = pro.prodName,
                    inStock = pro.inStock,
                    price = pro.price,
                    prodWeight = pro.prodWeight
                };

            return products;
        }

        public ProductDTO Post(Product pro)
        {
            pro.prodName = pro.prodName.Trim();
            db.Products.Add(pro);
            db.SaveChanges();

            ProductDTO p = new ProductDTO()
            {
                prodID = pro.prodID,
                prodName = pro.prodName,
                inStock = pro.inStock,
                price = pro.price,
                prodWeight = pro.prodWeight
            };

            return p;
        }

        public ProductDTO Put(int id, Product pro)
        {
            Product product = db.Products.FindAsync(id).Result;
            if (product == null) return null;
            pro.prodID = id;
            db.Entry(product).CurrentValues.SetValues(pro);
            db.SaveChangesAsync();

            ProductDTO p = new ProductDTO()
            {
                prodID = pro.prodID,
                prodName = pro.prodName,
                inStock = pro.inStock,
                price = pro.price,
                prodWeight = pro.prodWeight
            };

            return p;
        }

        public void Delete(int id)
        {
            db.Products.Remove(db.Products.FindAsync(id).Result);
            db.SaveChanges();

            // Add in logic to delete Cart records with prodId of id
        }
    }
}
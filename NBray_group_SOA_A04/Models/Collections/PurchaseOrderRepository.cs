﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */
using System;
using System.Collections.Generic;
using System.Linq;
using NBray_group_SOA_A04.DAL;

namespace NBray_group_SOA_A04.Models
{
    public class PurchaseOrderRepository
    {
        private ShoppingContext db = new ShoppingContext();

        public IQueryable<HeaderDTO> Get(int orderId, int custId, string firstName, string lastName, string poNumber,
            DateTime orderDate)
        {
            //Use DAL to query for the correct PO's
            IQueryable<HeaderDTO> po = from o in db.Orders
                    .Where(p => (orderId == 0 || p.orderId == orderId) &&
                                (custId == 0 || p.custId == custId) &&
                                (orderDate == DateTime.MinValue || p.orderDate == orderDate) &&
                                (poNumber == null || p.poNumber == poNumber) &&
                                (firstName == null || p.Customer.firstName == firstName) &&
                                (lastName == null || p.Customer.lastName == lastName))
                select new HeaderDTO()
                {
                    orderId = o.orderId,
                    custId = o.custId,
                    poNumber = o.poNumber,
                    orderDate = o.orderDate,
                    firstName = o.Customer.firstName,
                    lastName = o.Customer.lastName,
                    phoneNumber = o.Customer.phoneNumber
                };


            return po;
        }


        public PurchaseOrderDTO Get(int orderId)
        {
            PurchaseOrderDTO po = new PurchaseOrderDTO()
            {
                header = GetHeader(orderId),
                body = GetBody(orderId)
            };

            return po;
        }

        private HeaderDTO GetHeader(int orderId)
        {
            HeaderDTO header = db.Orders.Select(order =>
                new HeaderDTO()
                {
                    orderId = order.orderId,
                    custId = order.custId,
                    poNumber = order.poNumber,
                    orderDate = order.orderDate,
                    firstName = order.Customer.firstName,
                    lastName = order.Customer.lastName,
                    phoneNumber = order.Customer.phoneNumber
                }).SingleOrDefault(h => h.orderId == orderId);

            return header;
        }

        private BodyDTO GetBody(int orderId)
        {
            List<PurchaseOrderLineDTO> polList = GetOrderLines(orderId).ToList();

            float subtotal = 0.0f;
            int totalQuantity = 0;
            float totalWeight = 0.0f;

            foreach (PurchaseOrderLineDTO pol in polList)
            {
                totalQuantity += pol.quantity;
                subtotal += pol.price * pol.quantity;
                if (pol.inStock)
                {
                    totalWeight += pol.prodWeight * pol.quantity;
                }
            }

            float tax = subtotal * 0.13f;

            BodyDTO body = new BodyDTO
            {
                purchaseOrderLineDTOs = polList.ToList(),
                subtotal = subtotal,
                tax = tax,
                total = subtotal + tax,
                totalQuantity = totalQuantity,
                totalWeight = totalWeight
            };

            return body;
        }

        private IQueryable<PurchaseOrderLineDTO> GetOrderLines(int orderId)
        {
            IQueryable<PurchaseOrderLineDTO> pol = from cart in db.Carts
                    .Where(c => c.orderId == orderId)
                select new PurchaseOrderLineDTO
                {
                    prodID = cart.prodId,
                    prodName = cart.Product.prodName,
                    price = cart.Product.price,
                    prodWeight = cart.Product.prodWeight,
                    inStock = cart.Product.inStock,
                    quantity = (cart.Product.inStock ? cart.quantity : 0)
                };

            return pol;
        }
    }
}
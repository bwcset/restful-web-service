﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

namespace NBray_group_SOA_A04.Models
{
    /// <summary>
    /// This object represents an OrderLine object (labeled cart per specification) in which
    /// each line is an amount of Products for a given Order 
    /// </summary>
    public class Cart
    {
        /// <summary>
        /// The identifier if the Order
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// The identifier if the Product
        /// </summary>
        public int prodId { get; set; }

        /// <summary>
        /// The quantity of a Product for an Order
        /// </summary>
        public int quantity { get; set; }

        /// <summary>
        /// The related Order object that this item belongs to
        /// </summary>
        public virtual Order Order { get; set; }

        /// <summary>
        /// The related Product object that this item tracks the quantity of for an Order
        /// </summary>
        public virtual Product Product { get; set; }
    }
}
﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace NBray_group_SOA_A04.Models
{
    /// <summary>
    /// The Customer class defines the Customer object and will validate the fields to ensure
    /// that Crazy Melvin's Customer's are valid per specification.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// The unique identifier of a Customer
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int custId { get; set; }

        /// <summary>
        /// The First Name of the Customer
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// The Last Name of the Customer
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// The Phone Number of the Customer in the 'xxx-xxx-xxxx' format
        /// </summary>
        public string phoneNumber { get; set; }

        /// <summary>
        /// The relationship to Orders. i.e. Customers can have many Orders
        /// </summary>
        public virtual ICollection<Order> Orders { get; set; }


        /// <summary>
        /// This method will take a non-empty last name and verify it's length and format.
        /// </summary>
        /// <param name="name">The last name of the customer.</param>
        /// <returns>[ShopError: The error object returned if the product name is invalid.]
        /// [null: if it is valid]</returns>
        public static ShopError ValidateLastNameField(string name)
        {
            ShopError err = null;
            if (string.IsNullOrWhiteSpace(name))
            {
                err = new ShopError(216);
            }
            else if (name.Trim().Length > 50)
            {
                err = new ShopError(218);
            }
            else if (!Regex.IsMatch(name, @"^[A-Za-z,-.\s()]*$"))
            {
                err = new ShopError(220);
            }

            return err;
        }


        /// <summary>
        /// This method will take a non-empty first name and verify it's length and format.
        /// </summary>
        /// <param name="name">The first name of the customer.</param>
        /// <returns>[ShopError: The error object returned if the product name is invalid.]
        /// [null: if it is valid]</returns>
        public static ShopError ValidateFirstNameField(string name)
        {
            ShopError err = null;
            if (!string.IsNullOrWhiteSpace(name) && name.Trim().Length > 50)
            {
                err = new ShopError(217);
            }
            else if (!string.IsNullOrWhiteSpace(name) && !Regex.IsMatch(name, @"^[A-Za-z,-.\s()]*$"))
            {
                err = new ShopError(219);
            }

            return err;
        }


        /// <summary>
        /// This method will check the phone number against a regular expression to compare the format.
        /// </summary>
        /// <param name="phoneNumber">The phone number of the User</param>
        /// <returns>[ShopError: The error object returned if the product name is invalid.]
        /// [null: if it is valid]</returns>
        public static ShopError ValidatePhoneNumber(string phoneNumber)
        {
            ShopError err = null;
            if (string.IsNullOrWhiteSpace(phoneNumber))
            {
                err = new ShopError(221);
            }
            else if (!Regex.IsMatch(phoneNumber, @"^\d{3}-{0,1}\d{3}-{0,1}\d{4}$"))
            {
                err = new ShopError(222);
            }

            return err;
        }
    }
}
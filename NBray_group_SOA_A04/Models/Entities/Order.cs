﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBray_group_SOA_A04.Models
{
    /// <summary>
    /// The Order class is the object for when a user makes an Order of a Product.
    /// </summary>
    public class Order
    {
        /// <summary>
        /// This unique identifier is the primary key of Order
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int orderId { get; set; }

        /// <summary>
        /// The ID of the Customer this Order belongs to.
        /// </summary>
        public int custId { get; set; }

        /// <summary>
        /// The purchase order number that may or may not exist on an Order.
        /// </summary>
        public string poNumber { get; set; }

        /// <summary>
        /// The date at which the order took place.
        /// </summary>
        public DateTime orderDate { get; set; }

        /// <summary>
        /// The relationship to the Customer that the Order belongs to.
        /// </summary>
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// The relationship to the many Carts the Order can be referenced in.
        /// </summary>
        public virtual ICollection<Cart> Carts { get; set; }


        /// <summary>
        /// This method validates the Order Date in the request to determine if it is valid
        /// </summary>
        /// <param name="date">The date at which the order took place.</param>
        /// <returns>[ShopError: The error object returned if the date is invalid.]
        /// [null: if it is valid]</returns>
        public static ShopError ValidateDate(DateTime date)
        {
            ShopError err = null;
            if (date == DateTime.MinValue)
            {
                err = new ShopError(225);
            }
            else if (date > DateTime.Now)
            {
                err = new ShopError(224);
            }

            return err;
        }


        /// <summary>
        /// This method validates the Order P.O. Number in the request to determine if it is valid
        /// </summary>
        /// <param name="poNumber">the P.O. Number to check the length of.</param>
        /// <returns>[ShopError: The error object returned if the poNumber is invalid.]
        /// [null: if it is valid]</returns>
        public static ShopError ValidatePoNumber(string poNumber)
        {
            ShopError err = null;
            if (!string.IsNullOrWhiteSpace(poNumber) && poNumber.Trim().Length > 30)
            {
                err = new ShopError(227);
            }

            return err;
        }
    }
}
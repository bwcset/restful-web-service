﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBray_group_SOA_A04.Models
{
    /// <summary>
    /// The Product class defines the definition of what a Product object should be structured as
    /// and how some of the fields will need to be validated with.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// The unique identifier of a Product.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int prodID { get; set; }

        /// <summary>
        /// The Name of the Product.
        /// </summary>
        public string prodName { get; set; }

        /// <summary>
        /// The Price of the Product rounded to two decimal places.
        /// </summary>
        public float price { get; set; }

        /// <summary>
        /// The Weight of the Product in kgs.
        /// </summary>
        public float prodWeight { get; set; }

        /// <summary>
        /// The indicator for whether or not a given Product is in stock.
        /// </summary>
        public bool inStock { get; set; }

        /// <summary>
        /// The relationship to the many Carts the Product can be referenced in.
        /// </summary>
        public virtual ICollection<Cart> Carts { get; set; }


        /// <summary>
        /// This method validates the product name in the request to determine if it is valid
        /// </summary>
        /// <param name="p">The Product object to validate the name of.</param>
        /// <returns>[ShopError: The error object returned if the product name is invalid.]
        /// [null: if it is valid]</returns>
        public static ShopError ValidateProductName(Product p)
        {
            ShopError err = null;
            if (string.IsNullOrWhiteSpace(p.prodName))
            {
                // null check
                err = new ShopError(205);
            }
            else if (p.prodName.Trim().Length > 100)
            {
                // Length check
                err = new ShopError(206);
            }
            else if (int.TryParse(p.prodName, out int result))
            {
                // Redundant integer check
                err = new ShopError(214);
            }

            return err;
        }

        /// <summary>
        /// This method validates the product in the request to determine if it is valid
        /// </summary>
        /// <param name="p">The Product object to validate the price and weight of.</param>
        /// <returns>[List of ShopError: The error objects returned if the product is invalid.]
        /// [Empty List: if it is valid]</returns>
        public static List<ShopError> ValidateFloats(Product p)
        {
            List<ShopError> errList = new List<ShopError>();
            if (Math.Round(p.price, 2) < 0.01)
            {
                errList.Add(new ShopError(207));
            }

            if (p.prodWeight <= 0.00)
            {
                errList.Add(new ShopError(208));
            }

            return errList;
        }
    }
}
﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

using System;
using System.IO;
using System.Web;

namespace NBray_group_SOA_A04.Models
{
    /// <summary>
    /// This class is used to easily create an object within the API to store our custom errors
    /// </summary>
    public class ShopError
    { 

        /// <summary>
        /// The custom error code used to identify our errors
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// The error message stored in the global resources
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// The constructor that is used to make a message to respond to the client with
        /// using a global resource
        /// </summary>
        /// <param name="errorCode">The number on the end of the name of the ERROR string name</param>
        public ShopError(int errorCode)
        {

            ErrorMessage = HttpContext.GetGlobalResourceObject("ErrorCodes", "ERROR" + errorCode)?.ToString();
            ErrorCode = errorCode;

            try
            {
                Logging.WriteToLog(ErrorMessage);
            }catch(Exception e)
            {

            }

            //if (!File.Exists(path))
            //{
            //    // Create a file to write to.
            //    using (StreamWriter sw = File.CreateText(path))
            //    {
            //        sw.WriteLine("Logfile Created: " + DateTime.Today.ToString());
            //    }
            //}

            //// Open the file to read from.
            //using (StreamWriter sw = File.AppendText(path))
            //{
            //    sw.WriteLine(DateTime.Today.ToString() + "  -  Error: " + ErrorMessage );
            //}
        }
    }
}
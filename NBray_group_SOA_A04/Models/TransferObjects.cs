﻿/*
 * PROJECT      : RESTful Web Service
 * DEVELOPERS   : Nathan Bray, Carson Kyte, Gabriel Paquette
 * CREATED DATE : 2018-11-08
 * DESCRIPTION  : This file contains all the Data Transfer Objects.
 *                To minimize the redundant comments, this description encompasses all of them.
 *                Each of these classes are objects that the server side use to send the data to the client
 *                
 */

using System;
using System.Collections.Generic;

namespace NBray_group_SOA_A04.Models
{
    public class CustomerDTO
    {
        public int custId { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string phoneNumber { get; set; }
    }


    public class OrderDTO
    {
        public int orderId { get; set; }

        public int? custId { get; set; }

        public string poNumber { get; set; }

        public DateTime? orderDate { get; set; }
    }


    public class ProductDTO
    {
        public int prodID { get; set; }

        public string prodName { get; set; }

        public float? price { get; set; }

        public float? prodWeight { get; set; }

        public bool? inStock { get; set; }
    }


    public class CartDTO
    {
        public int orderId { get; set; }

        public int prodId { get; set; }

        public int quantity { get; set; }
    }


    public class CustOrderDTO
    {
        public int orderId { get; set; }

        public int? custId { get; set; }

        public string poNumber { get; set; }

        public string orderDate { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string phoneNumber { get; set; }
    }


    public class PurchaseOrderLineDTO
    {
        public int prodID { get; set; }

        public string prodName { get; set; }

        public float price { get; set; }

        public float prodWeight { get; set; }

        public bool inStock { get; set; }

        public int quantity { get; set; }
    }


    public class PurchaseOrderDTO
    {
        public HeaderDTO header { get; set; }

        public BodyDTO body { get; set; }
    }


    public class HeaderDTO
    {
        public int orderId { get; set; }

        public int custId { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string phoneNumber { get; set; }

        public string poNumber { get; set; }

        public DateTime? orderDate { get; set; }
    }


    public class BodyDTO
    {
        public List<PurchaseOrderLineDTO> purchaseOrderLineDTOs { get; set; }

        public float subtotal { get; set; }

        public float tax { get; set; }

        public float total { get; set; }

        public float totalWeight { get; set; }

        public int totalQuantity { get; set; }
    }
}
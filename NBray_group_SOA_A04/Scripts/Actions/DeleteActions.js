﻿

jQuery('#customerID').on('input', function () {
    if ($("#customerID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#custSubmit").attr("disabled", showFlag);
});

jQuery('#prodID').on('input', function () {
    if ($("#prodID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#prodSubmit").attr("disabled", showFlag);
});

jQuery('#orderID').on('input', function () {
    if ($("#orderID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#orderSubmit").attr("disabled", showFlag);
});


jQuery('#cartOrderID').on('input', function () {
    if ($("#cartOrderID").val().trim() == "" || $("#cartProdID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#cartSubmit").attr("disabled", showFlag);
});

jQuery('#cartProdID').on('input', function () {
    if ($("#cartOrderID").val().trim() == "" || $("#cartProdID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#cartSubmit").attr("disabled", showFlag);

});

function DeleteItem(routeAPI, itemFlag) {
    var oldRoute = routeAPI;
    if (itemFlag == 0) {
        if ($("#customerID").val().trim() != "") {
            routeAPI += $("#customerID").val().trim()
        }
    } else if (itemFlag == 1) {
        if ($("#prodID").val().trim() != "") {
            routeAPI += $("#prodID").val().trim()
        }
    }
    if (itemFlag == 2) {
        if ($("#orderID").val().trim() != "") {
            routeAPI += $("#orderID").val().trim()
        }
    } else if (itemFlag == 3) {
        if ($("#cartOrderID").val().trim() != "" && $("#cartProdID").val().trim() != "") {
            routeAPI += $("#cartOrderID").val().trim() + "/"
            routeAPI += $("#cartProdID").val().trim() + "/"
        }
    }

    if (routeAPI != oldRoute) {
        $('#FailBox').hide();
        $.ajax({
            url: routeAPI,
            type: 'DELETE',
            contentType: "application/json",
            success: function () {
                $('#FailBox').hide();
                $('#SuccessList').empty();

                var message = "Item successfully deleted!";
                $('<li/>', { text: message }).appendTo($('#SuccessList'));
                $('#SuccessBox').show();
            },
            fail: function () {
                $('#SuccessBox').hide();
                $('#FailList').empty();

                var message = "Failed to delete item.";
                $('<li/>', { text: message }).appendTo($('#FailList'));
                $('#FailList').show();

            }
        });
    } else {
        $('#FailList').empty();
        $('<li/>', { text: "Invalid ID" }).appendTo($('#FailList'));
        $('#FailBox').show();
    }
}

function showDeleteForm() {
    if ($("#selectList").val() == "Customer") {
        $("#CustomerDeleteForm").show();
        $("#ProductDeleteForm").hide();
        $("#OrderDeleteForm").hide();
        $("#CartDeleteForm").hide();
    }
    else if ($("#selectList").val() == "Product") {
        $("#CustomerDeleteForm").hide();
        $("#ProductDeleteForm").show();
        $("#OrderDeleteForm").hide();
        $("#CartDeleteForm").hide();
    }
    else if ($("#selectList").val() == "Order") {
        $("#CustomerDeleteForm").hide();
        $("#ProductDeleteForm").hide();
        $("#OrderDeleteForm").show();
        $("#CartDeleteForm").hide();
    }
    else if ($("#selectList").val() == "Cart") {
        $("#CustomerDeleteForm").hide();
        $("#ProductDeleteForm").hide();
        $("#OrderDeleteForm").hide();
        $("#CartDeleteForm").show();
    }

}
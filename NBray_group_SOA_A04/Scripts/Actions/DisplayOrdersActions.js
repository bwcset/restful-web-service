﻿$(document).ready(function () {
    $("#orderTable > tbody > tr").on('click', function (){
        GetPO("/api/purchaseOrders/", $(this).children('td').first().html());
    });
});

function GetPO(routeAPI, id) {
    routeAPI += id + "/";
    $.get(routeAPI).done(function (data) {
        if (Array.isArray(data)) {
            data = data[0];
        }
        console.log(data);
        $.get("/Actions/DisplayOrdersSearchBody", { orderId: id }).done(function () {
            window.location.href = "/Actions/Results"
        });
    })
}


function getDate(date) {
    var tempDate = new Date(date);
    var day = ("0" + tempDate.getDate()).slice(-2);
    var month = ("0" + tempDate.getMonth()).slice(-2);
    var year = ("0" + tempDate.getFullYear() % 1000).slice(-2);

    return formattedDate = (month + "-" + day + "-" + year).toString();
}
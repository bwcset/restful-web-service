﻿
function AddItem(apiRoute, itemFlag) {
    
    if (itemFlag == 0) {
        var item = {
            firstName: $("#fName").val(),
            lastName: $("#lName").val(),
            phoneNumber: $("#pNum").val()
        };
    } else if (itemFlag == 1) {
        var item = {
            prodName: $("#prodName").val(),
            prodWeight: $("#prodWeight").val(),
            price: $("#prodPrice").val(),
            inStock: $('#inStock').is(':checked')
        };
    } else if (itemFlag == 2) {
        var item = {
            custID: $("#orderCustomerID").val(),
            orderDate: $("#orderDate").val(),
            poNumber: $("#poNumber").val()
        };
    } else if (itemFlag == 3) {
        var item = {
            orderID: $("#cartOrderID").val(),
            prodID: $("#cartProdID").val(),
            quantity: $("#quanity").val(),
        };
    }


    $.post(apiRoute, item).done(function (data) {
        $('#FailBox').hide();
        $('#SuccessList').empty();

        var message = "Item added successfully!";
        $('<li/>', { text: message }).appendTo($('#SuccessList'));
        $('#SuccessBox').show();

        console.log(data);

        if (itemFlag == 0) {
            var str = (data.firstName != null ? data.firstName : "") + ' ' + data.lastName + ': ' + data.phoneNumber;
        } else if (itemFlag == 1) {
            var str = "ID " + data.prodID + ": " + data.prodName + '   $' + data.price;
        } else if (itemFlag == 2) {
            var str = "Order ID :" + data.orderId + "    Date :" + data.orderDate + '     PO Number :' + data.poNumber;
        } else if (itemFlag == 3) {
            var str = "Order ID :" + data.orderId + "    Product ID :" + data.prodId + '     $' + data.quantity;
        }

        console.log("tests");
        $('#items').empty();
        $('<li />', { text: str }).appendTo($('#items'));

    }).fail(function (data) {
        $('#SuccessBox').hide();
        $('#FailList').empty();
        var response = data.responseJSON

        if (response.message) {
            //exception was thrown
        } else {
            $.each(response, function (key, val) {
                console.log(val);
                var errorResponse = val.ErrorMessage;
                $('<li/>', { text: errorResponse }).appendTo($('#FailList'));
            });
        }
        $('#FailBox').show();
    });
}


function showInsertForm() {
    if ($("#selectList").val() == "Customer") {
        $("#CustomerAddForm").show();
        $("#ProductAddForm").hide();
        $("#OrderAddhForm").hide();
        $("#CartAddForm").hide();
    }
    else if ($("#selectList").val() == "Product") {
        $("#CustomerAddForm").hide();
        $("#ProductAddForm").show();
        $("#OrderAddhForm").hide();
        $("#CartAddForm").hide();
    }
    else if ($("#selectList").val() == "Order") {
        $("#CustomerAddForm").hide();
        $("#ProductAddForm").hide();
        $("#OrderAddForm").show();
        $("#CartAddForm").hide();
    }
    else if ($("#selectList").val() == "Cart") {
        $("#CustomerAddForm").hide();
        $("#ProductAddForm").hide();
        $("#OrderAddForm").hide();
        $("#CartAddForm").show();
    }

}
        
﻿
$(document).ready(function () {
    $("#selectList").val("Customer");
});



jQuery('#customerID').on('input', function () {
    if ($("#customerID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#custSubmit").attr("disabled", showFlag);
});

jQuery('#prodID').on('input', function () {
    if ($("#prodID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#prodSubmit").attr("disabled", showFlag);
});


jQuery('#orderID').on('input', function () {
    if ($("#orderID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#orderSubmit").attr("disabled", showFlag);
});


jQuery('#cartOrderID').on('input', function () {
    if ($("#cartOrderID").val().trim() == "" || $("#cartProdID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#cartSubmit").attr("disabled", showFlag);
});

jQuery('#cartProdID').on('input', function () {
    if ($("#cartOrderID").val().trim() == "" || $("#cartProdID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#cartSubmit").attr("disabled", showFlag);

});


function SearchID(routeAPI, itemFlag, url) {
    var oldRoute = routeAPI;
    if (itemFlag == 0) {
        if ($("#customerID").val().trim() != "") {
            routeAPI += $("#customerID").val().trim()
        }
    } else if (itemFlag == 1) {
        if ($("#prodID").val().trim() != "") {
            routeAPI += $("#prodID").val().trim()
        }
    }
    if (itemFlag == 2) {
        if ($("#orderID").val().trim() != "") {
            routeAPI += $("#orderID").val().trim()
        }
    } else if (itemFlag == 3) {
        if ($("#cartOrderID").val().trim() != "" && $("#cartProdID").val().trim() != "") {
            routeAPI += $("#cartOrderID").val().trim() + "/"
            routeAPI += $("#cartProdID").val().trim() + "/"
        }
    }

    console.log(url);
    if (oldRoute != routeAPI) {
        $.get(routeAPI).done(function (data) {
            $.get(url, data).done(function () {
               window.location.href =  window.location.href.replace("PreUpdateSearch", "Update");
            });
        }).fail(function (data) {
            $('#FailList').empty();
            $('<li/>', { text: "Failed to find item" }).appendTo($('#FailList'));
            $('#FailBox').show();
        });
    } else {
        $('#FailList').empty();
        $('<li/>', { text: "Invalid search field" }).appendTo($('#FailList'));
        $('#FailBox').show();
    }
}


function showIDForm() {
    if ($("#selectList").val() == "Customer") {
        $("#CustomerIDSearchForm").show();
        $("#ProductIDSearchForm").hide();
        $("#OrderIDSearchForm").hide();
        $("#CartIDSearchForm").hide();
    }
    else if ($("#selectList").val() == "Product") {
        $("#CustomerIDSearchForm").hide();
        $("#ProductIDSearchForm").show();
        $("#OrderIDSearchForm").hide();
        $("#CartIDSearchForm").hide();
    }
    else if ($("#selectList").val() == "Order") {
        $("#CustomerIDSearchForm").hide();
        $("#ProductIDSearchForm").hide();
        $("#OrderIDSearchForm").show();
        $("#CartIDSearchForm").hide();
    }
    else if ($("#selectList").val() == "Cart") {
        $("#CustomerIDSearchForm").hide();
        $("#ProductIDSearchForm").hide();
        $("#OrderIDSearchForm").hide();
        $("#CartIDSearchForm").show();
    }

}
﻿


function GetPurchaseOrder(routeAPI, id) {
    routeAPI += id + "/";
    $.get(routeAPI).done(function (data) {
        if (Array.isArray(data)) {
            data = data[0];
        }
        console.log(data);

        var date = getDate(data.header.orderDate);

        $("#resultsPurchaseDate").append(date)
        $("#resultsPONumber").append(data.header.poNumber)
        $("#resultsCustID").append(data.header.custId)
        $("#resultsCustName").append(data.header.lastName + ", " + (data.header.firstName != null ? data.header.firstName : ""))
        $("#resultsCustpNumber").append(data.header.phoneNumber)

        $("#reusltsSubTotal").append(data.body.subtotal)
        $("#resultsTax").append(data.body.tax)
        $("#resultsTotal").append(data.body.total)

        $.each(data.body.purchaseOrderLineDTOs, function (index, val) {     
            if (!val.inStock) {
                var firstTR = '<tr class="danger">';
            }
            else {
                var firstTR = '<tr>';
            }
            $("#orderProductTable").append(firstTR
                + '<td>' + val.prodID + '</td>'
                + '<td>' + val.prodName + '</td>'
                + '<td>' + val.quantity + '</td>'
                + '<td>' + val.price + '</td>'
                + '<td>' + val.prodWeight + '</td>'
                + '</tr>');
        });

        $("#resultsTotalPieces").append(data.body.totalQuantity);
        $("#resultsTotalWeight").append(Number.parseFloat(data.body.totalWeight).toFixed(2));


    })
}

function getDate(date) {
    var tempDate = new Date(date);
    var day = ("0" + tempDate.getDate()).slice(-2);
    var month = ("0" + tempDate.getMonth()).slice(-2);
    var year = ("0" + tempDate.getFullYear() % 1000).slice(-2);

    return formattedDate = (day + "-" + month + "-" + year).toString();
}

﻿
function SearchItem(apiRoute, itemFlag, url = "") {
    var oldRoute = apiRoute;
    var validApi = false;
    var singleDisplay = false;


    if (itemFlag == 0) {
        if ($("#searchCustomerID").val().trim() != "") {
            apiRoute += $("#searchCustomerID").val().trim();
            singleDisplay = true;
        } else {
            apiRoute += ($("#searchfName").val().trim() == "" ? 0 : $("#searchfName").val().trim()) + "/";
            apiRoute += ($("#searchlName").val().trim() == "" ? 0 : $("#searchlName").val().trim()) + "/";
            apiRoute += $("#searchpNum").val().trim() == "" ? 0 : $("#searchpNum").val().trim();
        }

        if (apiRoute != oldRoute + "0/0/0") {
            validApi = true;
        }   
    } else if (itemFlag == 1) {

        if ($("#searchProdID").val().trim() != "") {
            apiRoute += $("#searchProdID").val().trim()
            singleDisplay = true;
        } else if ($("#searchProdName").val().trim() != "" && isNaN($("#searchProdName").val().trim())) {
            apiRoute += $("#searchProdName").val().trim();
        } else if ($("#searchInStock").val().trim() != "" && $("#searchInStock").val().trim() != "N/A") {
            apiRoute += $("#searchInStock").val().trim();
        }

        if (apiRoute != oldRoute) {
            //a valid api route has been made
            validApi = true;
        }
    } else if (itemFlag == 2) {

        if ($("#searchOrderID").val().trim() != "" && !isNaN($("#searchOrderID").val().trim())) {
            apiRoute += $("#searchOrderID").val().trim()
            singleDisplay = true;
        } else if ($("#searchOrderCustomerID").val().trim() != "" && !isNaN($("#searchOrderCustomerID").val().trim())) {
            apiRoute += ($("#searchOrderCustomerID").val().trim() == "" ? 0 : $("#searchOrderCustomerID").val().trim()) + "/0/0";
        } else {
            apiRoute += "0/";
            apiRoute += ($("#searchOrderDate").val().trim() == "" ? 0 : $("#searchOrderDate").val().trim()) + "/";
            apiRoute += $("#searchpoNumber").val().trim() == "" ? 0 : $("#searchpoNumber").val().trim();
        }

        if (apiRoute != oldRoute + "0/0/0") {
            //a valid api route has been made
            validApi = true;
        }
    } else if (itemFlag == 3) {

        if ($("#searchCartOrderID").val().trim() != "" && !isNaN($("#searchCartOrderID").val().trim())) {
            apiRoute += $("#searchCartOrderID").val().trim() + "/";
            singleDisplay = true;
        }
        else {
            apiRoute += "0/";
        }
        if ($("#searchCartProdID").val().trim() != "" && !isNaN($("#searchCartProdID").val().trim())) {
            apiRoute += $("#searchCartProdID").val().trim();
        }
        else {
            apiRoute += "0";
            singleDisplay = false;
        }
        
        if (apiRoute != oldRoute + "0/0") {
            //a valid api route has been made
            validApi = true;
        }
    }
    else if (itemFlag == 4) {
        //[Route("custOrders/{custId}/{poNumber}/{orderDate}/{firstName}/{lastName}/{phoneNumber}")]
        apiRoute += ($("#searchCustomerOrderID").val().trim() || "0") + "/"
        apiRoute += ($("#searchCustOrderpoNumber").val().trim() || "0") + "/"
        apiRoute += ($("#searchCustOrderDate").val().trim() || "0") + "/"
        apiRoute += ($("#searchCustOrderfName").val().trim() || "0") + "/"
        apiRoute += ($("#searchCustOrderlName").val().trim() || "0") + "/"
        apiRoute += ($("#searchCustOrderpNum").val().trim() || "0")
        
        
        
        if (apiRoute != oldRoute + "0/0/0/0/0/0") {
            //a valid api route has been made
            validApi = true;
        }
    }


    if (validApi) {
        $('#FailBox').hide();
        $('#FailList').empty();

        $('#SuccessBox').hide();
        $('#SuccessList').empty();

        $("#itemPannel").hide();
        $("#multiItem").hide();
        $("#itemPannelBody").find("label").remove();
        $("#itemPannelBody").find("br").remove();

        

        $.get(apiRoute).done(function (data) {
            console.log(data);


            if ($("#generatePOCheck").is(':checked')) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({ customerOrders: data }),
                    success: (function() {
                        window.location.href = window.location.href.replace("Search", "DisplayOrders");
                    })
                });
            } else {
                
                if (Array.isArray(data)) {
                    if (data.length == 1) {
                        data = data[0];
                        singleDisplay = true;
                    }
                }
                var itemType = getItemType(itemFlag);

                $("#itemPannel").show();
                if (singleDisplay) {
                    $("#itemTypePannelHeader").html(itemType)
                    $("#itemPannelBody").append(createSinglePannelData(data, itemFlag));
                } else {

                    $("#itemTypePannelHeader").html(itemType + " - " + data.length)
                    createMultiPannelData(data, itemFlag);
                }
            }
            
        }).fail(function (data) {
            console.log(data);
            console.log("fail");
            $("#itemPannel").hide();
            
            var response = data.responseJSON
            if (response.message) {
                //exception was thrown
            } else {
                $.each(response, function (key, val) {
                    console.log(val);
                    var errorResponse = val.ErrorMessage;
                    $('<li/>', { text: errorResponse }).appendTo($('#FailList'));
                });
            }
            $('#FailBox').show();
        });
    }
    else {
        $('#FailList').empty();
        $('<li/>', { text: "Invalid search field" }).appendTo($('#FailList'));
        $('#FailBox').show();
    }
}




function createMultiPannelData(data, itemFlag) {
    $("#headerRow").empty();
    $("#tableBody").empty();

    $("#multiItem").show();
    switch (itemFlag) {
        case 0:
            var headerRow = document.getElementById("headerRow");
            var cell = headerRow.insertCell(0); 
            cell.innerHTML = "<strong>Customer ID</strong>";
            cell = headerRow.insertCell(1);
            cell.innerHTML = "<strong>First Name</strong>";
            cell = headerRow.insertCell(2);
            cell.innerHTML = "<strong>Last Name</strong>";
            cell = headerRow.insertCell(3);
            cell.innerHTML = "<strong>Phone Number</strong>";

            var tb = document.getElementById("tableBody");
            
            for (var i = 0; i < data.length; i++) {
                var row = tb.insertRow(i);
                var cell = row.insertCell(0);
                cell.innerHTML = (data[i].custId);
                cell = row.insertCell(1);
                cell.innerHTML = (data[i].firstName || "");
                cell = row.insertCell(2);
                cell.innerHTML = (data[i].lastName);
                cell = row.insertCell(3);
                cell.innerHTML = (data[i].phoneNumber);
            }
            break;
        case 1:
            var headerRow = document.getElementById("headerRow");
            var cell = headerRow.insertCell(0);
            cell.innerHTML = "<strong>Product ID</strong>";
            cell = headerRow.insertCell(1);
            cell.innerHTML = "<strong>Produt Name</strong>";
            cell = headerRow.insertCell(2);
            cell.innerHTML = "<strong>Product Price</strong>";
            cell = headerRow.insertCell(3);
            cell.innerHTML = "<strong>Product Weight</strong>";
            cell = headerRow.insertCell(4);
            cell.innerHTML = "<strong>In Stock</strong>";

            var tb = document.getElementById("tableBody");

            for (var i = 0; i < data.length; i++) {
                var row = tb.insertRow(i);
                var cell = row.insertCell(0);
                cell.innerHTML = (data[i].prodID);
                cell = row.insertCell(1);
                cell.innerHTML = (data[i].prodName);
                cell = row.insertCell(2);
                cell.innerHTML = (data[i].price);
                cell = row.insertCell(3);
                cell.innerHTML = (data[i].prodWeight);
                cell = row.insertCell(4);
                cell.innerHTML = (data[i].inStock);
            }
            break;
        case 2:
            var headerRow = document.getElementById("headerRow");
            var cell = headerRow.insertCell(0);
            cell.innerHTML = "<strong>Order ID</strong>";
            cell = headerRow.insertCell(1);
            cell.innerHTML = "<strong>Customer ID</strong>";
            cell = headerRow.insertCell(2);
            cell.innerHTML = "<strong>Order Date</strong>";
            cell = headerRow.insertCell(3);
            cell.innerHTML = "<strong>P.O. Number</strong>";

            var tb = document.getElementById("tableBody");

            for (var i = 0; i < data.length; i++) {
                var row = tb.insertRow(i);
                var cell = row.insertCell(0);
                cell.innerHTML = (data[i].orderId);
                cell = row.insertCell(1);
                cell.innerHTML = (data[i].custId);
                cell = row.insertCell(2);
                cell.innerHTML = (getDate(data[i].orderDate));
                cell = row.insertCell(3);
                cell.innerHTML = (data[i].poNumber || "");
            }
            break;
        case 3:
            var headerRow = document.getElementById("headerRow");
            var cell = headerRow.insertCell(0);
            cell.innerHTML = "<strong>Order ID</strong>";
            cell = headerRow.insertCell(1);
            cell.innerHTML = "<strong>Product ID</strong>";
            cell = headerRow.insertCell(2);
            cell.innerHTML = "<strong>Quantity</strong>";

            var tb = document.getElementById("tableBody");

            for (var i = 0; i < data.length; i++) {
                var row = tb.insertRow(i);
                var cell = row.insertCell(0);
                cell.innerHTML = (data[i].orderId);
                cell = row.insertCell(1);
                cell.innerHTML = (data[i].prodId);
                cell = row.insertCell(2);
                cell.innerHTML = (data[i].quantity);
            }

            break;
        case 4:
            var headerRow = document.getElementById("headerRow");
            var cell = headerRow.insertCell(0);
            cell.innerHTML = "<strong>Customer ID</strong>";
            cell = headerRow.insertCell(1);
            cell.innerHTML = "<strong>First Name</strong>";
            cell = headerRow.insertCell(2);
            cell.innerHTML = "<strong>Last Name</strong>";
            cell = headerRow.insertCell(3);
            cell.innerHTML = "<strong>Phone Number</strong>";
            cell = headerRow.insertCell(4);
            cell.innerHTML = "<strong>Order ID</strong>";
            cell = headerRow.insertCell(5);
            cell.innerHTML = "<strong>Order Date</strong>";
            cell = headerRow.insertCell(6);
            cell.innerHTML = "<strong>P.O. Number</strong>";

            var tb = document.getElementById("tableBody");

            for (var i = 0; i < data.length; i++) {
                var row = tb.insertRow(i);
                var cell = row.insertCell(0);
                cell.innerHTML = (data[i].custId);
                cell = row.insertCell(1);
                cell.innerHTML = (data[i].firstName || "");
                cell = row.insertCell(2);
                cell.innerHTML = (data[i].lastName);
                cell = row.insertCell(3);
                cell.innerHTML = (data[i].phoneNumber);
                cell = row.insertCell(4);
                cell.innerHTML = (data[i].orderId);
                cell = row.insertCell(5);
                cell.innerHTML = (getDate(data[i].orderDate));
                cell = row.insertCell(6);
                cell.innerHTML = (data[i].poNumber || "");
            }

            break;
    }
    
}


function createSinglePannelData(data, itemFlag) {

    var pannelBody = "";

    switch (itemFlag) {
        case 0:
            pannelBody = '<label>Customer ID: ' + data.custId + '</label></br>'
            + '<label> First Name: ' + (data.firstName || "") + '</label></br>'
            + '<label> Last Name: ' + data.lastName + '</label></br>'
            + '<label> Phone Number: ' + data.phoneNumber + '</label>';
            break;
        case 1:
            pannelBody = '<label>Product ID: ' + data.prodID + '</label></br>'
                + '<label> Product Name: ' + data.prodName + '</label></br>'
                + '<label> Product Price: ' + data.price + '</label></br>'
                + '<label> Product Weight: ' + data.prodWeight + '</label></br>'
                + '<label> In Stock: ' + data.inStock + '</label>';
            break;
        case 2:
            pannelBody = '<label>Order ID: ' + data.orderId + '</label></br>'
                + '<label> Customer ID: ' + data.custId + '</label></br>'
                + '<label> Order Date: ' + getDate(data.orderDate) + '</label></br>'
                + '<label> P.O. Number: ' + (data.poNumber || "") + '</label>';
            break;
        case 3:
            pannelBody = '<label>Order ID: ' + data.orderId + '</label></br>'
                + '<label> Product ID: ' + data.prodId + '</label></br>'
                + '<label> Product Quantity: ' + data.quantity + '</label>'
            break;
    }

    return pannelBody;
}

function getItemType(itemFlag) {

    var name = "";

    switch (itemFlag) {
        case 0:
            name = "Customer";
            break;
        case 1:
            name = "Product";
            break;
        case 2:
            name = "Order";
            break;
        case 3:
            name = "Cart";
            break;
        case 4:
            name = "Customer/Order"
    }

    return name;
}


function getDate(date) {
    var tempDate = new Date(date);
    var day = ("0" + tempDate.getDate()).slice(-2);
    var month = ("0" + tempDate.getMonth()).slice(-2);
    var year = ("0" + tempDate.getFullYear() % 1000).slice(-2);

    return formattedDate = (day + "-" + month + "-" + year).toString();
}


function showSearchForm() {
    $("#checkBoxContainer").hide();

    if ($("#selectList").val() == "Customer") {
        $("#CustomerSearchForm").show();
        $("#ProductSearchForm").hide();
        $("#OrderSearchForm").hide();
        $("#CartSearchForm").hide();
        $("#CustomerOrderSearchForm").hide();
    }
    else if ($("#selectList").val() == "Product") {
        $("#CustomerSearchForm").hide();
        $("#ProductSearchForm").show();
        $("#OrderSearchForm").hide();
        $("#CartSearchForm").hide();
        $("#CustomerOrderSearchForm").hide();
    }
    else if ($("#selectList").val() == "Order") {
        $("#CustomerSearchForm").hide();
        $("#ProductSearchForm").hide();
        $("#OrderSearchForm").show();
        $("#CartSearchForm").hide();
        $("#CustomerOrderSearchForm").hide();
    }
    else if ($("#selectList").val() == "Cart") {
        $("#CustomerSearchForm").hide();
        $("#ProductSearchForm").hide();
        $("#OrderSearchForm").hide();
        $("#CartSearchForm").show();
        $("#CustomerOrderSearchForm").hide();
    }
    else if ($("#selectList").val() == "CustOrder") {
        $("#CustomerSearchForm").hide();
        $("#ProductSearchForm").hide();
        $("#OrderSearchForm").hide();
        $("#CartSearchForm").hide();
        $("#CustomerOrderSearchForm").show();
        $("#checkBoxContainer").show();
    }
}


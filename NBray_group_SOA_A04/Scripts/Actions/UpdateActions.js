﻿
jQuery('#updateCustomerID').on('input', function () {
    if ($("#updateCustomerID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#fName").prop("disabled", showFlag);
    $("#lName").prop("disabled", showFlag);
    $("#pNum").prop("disabled", showFlag);
});

jQuery('#updateProdID').on('input', function () {
    if ($("#updateProdID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#prodName").prop("disabled", showFlag);
    $("#prodPrice").prop("disabled", showFlag);
    $("#prodWeight").prop("disabled", showFlag);
    $("#inStock").prop("disabled", showFlag);

});

jQuery('#updateOrderID').on('input', function () {
    if ($("#updateOrderCustomerID").val().trim() == "" || $("#updateOrderID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#orderDate").prop("disabled", showFlag);
    $("#poNumber").prop("disabled", showFlag);

});

jQuery('#updateOrderCustomerID').on('input', function () {
    if ($("#updateOrderCustomerID").val().trim() == "" || $("#updateOrderID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#orderDate").prop("disabled", showFlag);
    $("#poNumber").prop("disabled", showFlag);

});

jQuery('#updateCartOrderID').on('input', function () {
    if ($("#updateCartProdID").val().trim() == "" || $("#updateCartOrderID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#quanity").prop("disabled", showFlag);

});

jQuery('#updateCartProdID').on('input', function () {
    if ($("#updateCartProdID").val().trim() == "" || $("#updateCartOrderID").val().trim() == "") {
        var showFlag = true;
    } else {
        var showFlag = false;
    }
    $("#quanity").prop("disabled", showFlag);

});


$(document).ready(function () {
    if ($('#updateCustomerID').val() == 0) {
        if ($('#updateProdID').val() == 0) {
            if ($("#updateOrderID").val() == 0) {
                if ($("#updateCartOrderID").val() != 0) {
                    $("#CustomerUpdateForm").hide();
                    $("#ProductUpdateForm").hide();
                    $("#OrderUpdateForm").hide();
                    $("#CartUpdateForm").show();
                }
            } else {
                $("#CustomerUpdateForm").hide();
                $("#ProductUpdateForm").hide();
                $("#OrderUpdateForm").show();
                $("#CartUpdateForm").hide();
            }
        } else {
            $("#CustomerUpdateForm").hide();
            $("#ProductUpdateForm").show();
            $("#OrderUpdateForm").hide();
            $("#CartUpdateForm").hide();
        }
    } else {
        $("#CustomerUpdateForm").show();
        $("#ProductUpdateForm").hide();
        $("#OrderUpdateForm").hide();
        $("#CartUpdateForm").hide();
    }
});

function UpdateItem(routeAPI, itemFlag) {
    var URL = routeAPI;
    if (itemFlag == 0) {
        URL += $("#updateCustomerID").val()
        var item = {
            custID: $("#updateCustomerID").val(),
            firstName: $("#fName").val(),
            lastName: $("#lName").val(),
            phoneNumber: $("#pNum").val()
        };

    } else if (itemFlag == 1) {
        URL += $("#updateProdID").val()
        var item = {
            prodID: $("#updateProdID").val(),
            prodName: $("#prodName").val(),
            prodWeight: $("#prodWeight").val(),
            price: $("#prodPrice").val(),
            inStock: $('#inStock').is(':checked')
        };
    } else if (itemFlag == 2) {
        URL += $("#updateOrderID").val()
        var item = {
            orderID: $("#updateOrderID").val(),
            custID: $("#updateOrderCustomerID").val(),
            orderDate: $("#orderDate").val(),
            poNumber: $('#poNumber').val()
        };
    } else if (itemFlag == 3) {
        URL += $("#updateCartOrderID").val() + "/" + $("#updateCartProdID").val();
        var item = {
            orderID: $("#updateCartOrderID").val(),
            prodId: $("#updateCartProdID").val(),
            quantity: $("#quantity").val(),
        };
    }

    console.log(URL);
    console.log(item);

    $.ajax({
        url: URL,
        type: 'PUT',
        data: JSON.stringify(item),
        contentType: "application/json"
    }).done(function (data) {
        $('#FailBox').hide();
        $('#SuccessList').empty();

        var message = "Item updated successfully!";
        $('<li/>', { text: message }).appendTo($('#SuccessList'));
        $('#SuccessBox').show();


        if (itemFlag == 0) {
            var str = (data.firstName || "") + ' ' + data.lastName + ': ' + data.phoneNumber;
        } else if (itemFlag == 1) {
            var str = "ID " + data.prodID + ": " + data.prodName + '   $' + data.price;
        } else if (itemFlag == 2) {
            var str = "Order ID :" + data.orderId + "    Date :" + data.orderDate + '     PO Number :' + (data.poNumber || "");
        } else if (itemFlag == 3) {
            var str = "Order ID :" + data.orderId + "    Product ID :" + data.prodId + '     $' + data.quantity;
        }

        $('#items').empty();
        $('<li />', { text: str }).appendTo($('#items'));
    }).fail(function (data) {
        $('#SuccessBox').hide();
        $('#FailList').empty();

        var response = data.responseJSON
        
        $.each(response, function (key, val) {
            console.log(val);
            var errorResponse = val.ErrorMessage;
            $('<li/>', { text: errorResponse }).appendTo($('#FailList'));
        });
        
        $('#FailBox').show();
    });
};
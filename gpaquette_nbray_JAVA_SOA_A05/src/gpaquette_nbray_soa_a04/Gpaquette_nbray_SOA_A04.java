package gpaquette_nbray_soa_a04;

/*
 * PROJECT      : Java Microservice logger
 * DEVELOPERS   : Nathan Bray Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */


import static gpaquette_nbray_soa_a04.Server.aesKey;
import static gpaquette_nbray_soa_a04.Server.cipher;
import static gpaquette_nbray_soa_a04.Server.fullFilePath;
import static gpaquette_nbray_soa_a04.Server.iv;
import static gpaquette_nbray_soa_a04.Server.logFile;
import static gpaquette_nbray_soa_a04.Server.validClientNames;
import static gpaquette_nbray_soa_a04.Server.maxMsgLength;



import javax.jws.WebService;
import java.io.*;
import java.util.*;
import javax.crypto.Cipher;

import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author gpaquette1144
 */


@WebService()
public class Gpaquette_nbray_SOA_A04 {

    static List<String> validGUIDS = new ArrayList<String>(); 
    
	/*
	 * Name      : logMessage
	 * CREATED DATE : 2018-11-08
	 * Description: Logs a message sent by the client 
	 */
    public boolean logMessage(String msg) throws Exception {
		
		//decrypt the message and save it to the log.
        cipher.init(Cipher.DECRYPT_MODE, aesKey, iv);
        byte[] original = cipher.doFinal(DatatypeConverter.parseBase64Binary(msg));
        String decrypted = new String(original);

        String[] messageParts = decrypted.split(";", 2);
		
		
        if(validGUIDS.contains(messageParts[0]) && messageParts[1].length() <= maxMsgLength){
            if(logFile.exists() && !logFile.isDirectory()){
                BufferedWriter output;
                output = new BufferedWriter(new FileWriter(fullFilePath, true));
                output.write(messageParts[1]);
                output.newLine();
                output.close();
                
                return true;
            }
        }
        
        return false;
    }
    
    /*
	 * Name      : generateToken
	 * CREATED DATE : 2018-11-08
	 * Description: Generates a token for the client to use to send messages back and forth.
	 */
    public String generateToken(String clientID) {
        
		//create a guid for the client, encrypt it and send it to the client.
        String guid = null;
        String decrypted = null;
        try{
            cipher.init(Cipher.DECRYPT_MODE, aesKey, iv);
            byte[] original = cipher.doFinal(DatatypeConverter.parseBase64Binary(clientID));
            decrypted = new String(original);
            
        }catch(Exception e){
            
        }
        
        if(validClientNames.contains(decrypted)) {
            guid = UUID.randomUUID().toString();
            validGUIDS.add(guid);
            
            try{
                cipher.init(Cipher.ENCRYPT_MODE , aesKey, iv);
                byte[] encrypted = cipher.doFinal(guid.getBytes());
                
                return Base64.getEncoder().encodeToString(encrypted);
            }catch(Exception e){

            }
        }
        return guid;
    }
}

package gpaquette_nbray_soa_a04;

/*
 * PROJECT      : Java Microservice logger
 * DEVELOPERS   : Nathan Bray Gabriel Paquette
 * CREATED DATE : 2018-11-08
 */

import javax.xml.ws.Endpoint;
import java.io.*;
import java.security.Key;
import java.text.*;
import java.util.*;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Server {

    static String address = "";
    static String filePath = "";
    static String fullFilePath = "";
    static int maxMsgLength = 0;
    static File logFile;
    static List<String> validClientNames = new ArrayList<String>(); 
    
    final static String key = "Bar12345Bar12345";
    final static String ivString = "RandomInitVector";
    
    static Key aesKey = null;
    static IvParameterSpec iv = null;
    static Cipher cipher = null;
    
    
	/*
	 * Name      : Server
	 * CREATED DATE : 2018-11-08
	 * Description: This function inits the server, and reads in the config file.
	 */
    protected Server() throws IOException  {
        File file = new File("config.txt"); 
		//read in the config file
        try(BufferedReader br = new BufferedReader(new FileReader(file))) { 
            address =  br.readLine();
            filePath = br.readLine();
            maxMsgLength = Integer.parseInt(br.readLine());

            do{
                validClientNames.add(br.readLine());
            }while(br.readLine() != null);

        }

		//create the log file
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        fullFilePath = filePath + "\\" + dateFormat.format(date)+ ".txt";
        
        logFile = new File(fullFilePath);
        if(!logFile.exists() && !logFile.isDirectory()){
            if(logFile.createNewFile()){
                BufferedWriter output;
                output = new BufferedWriter(new FileWriter(fullFilePath, true));
                output.write(fullFilePath+" File Created");
                output.newLine();
                output.close();
            }    
        }
        
		//start the server
        if( address != null)
        {        
            System.out.println("Starting Server");
            Object implementor = new Gpaquette_nbray_SOA_A04();
            Endpoint.publish(address, implementor);
        }
        
        //create the cipher for encryption and decryption
        aesKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        iv = new IvParameterSpec(ivString.getBytes("UTF-8"));
        
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            
        }catch(Exception e){
            
        }
        
    }

	    
	/*
	 * Name      : main
	 * CREATED DATE : 2018-11-08
	 * Description: Create a server
	 */
    public static void main(String args[]) throws Exception {
        new Server();
    }
}